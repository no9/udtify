var fs = require( 'fs' );
var falafel = require('falafel');

var walkPath = './';

var walk = function (dir, done) {
    fs.readdir(dir, function (error, list) {
        if (error) {
            return done(error);
        }

        var i = 0;

        (function next () {
            var file = list[i++];

            if (!file) {
                return done(null);
            }
            
            file = dir + '/' + file;
            
            fs.stat(file, function (error, stat) {
        
                if (stat && stat.isDirectory()) {
                    walk(file, function (error) {
                        next();
                    });
                } else {
                    // do stuff to file here
		     if(endsWith(file.toString(), '.js')) {
		     	console.log(file);
 			fs.readFile(file, function(err, data) {
         		if (err) { throw err; }
			data = data.toString().replace('#!/usr/bin/env node', '');
          		data = '{' + data + '}';
 
         		output = falafel(data, function (node) {
			   if(node.name === 'require') {
				node.update(node.parent.source().replace('(\'net\)', '(\'udt\')'));
				node.update(node.parent.source().replace('http', 'httpp'));
				node.update(node.parent.source().replace('tls', 'udts'));
				node.update(node.parent.source().replace('https', 'httpps'));
			   
              			//console.log(node);
              			console.log(node.parent.source());
			   }
            		})
		     })
		    }
                    next();
                }
            });
        })();
    });
};

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
// optional command line params
//      source for walk path
process.argv.forEach(function (val, index, array) {
    if (val.indexOf('source') !== -1) {
        walkPath = val.split('=')[1];
    }
});

console.log('-------------------------------------------------------------');
console.log('processing...');
console.log('-------------------------------------------------------------');

walk(walkPath, function(error) {
    if (error) {
        throw error;
    } else {
        console.log('-------------------------------------------------------------');
        console.log('finished.');
        console.log('-------------------------------------------------------------');
    }
});
